<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['asset-js'] = [
    // Add here the JS files that must be added on all pages
];
$config['asset-css'] = [
    // Add here the CSS files that must be added on all pages
];
