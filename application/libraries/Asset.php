<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('ComposerAdapter.php');

class Asset extends ComposerAdapter
{
    public function __construct()
    {
        parent::__construct(new SteeveDroz\CiAsset\Asset());
    }
}
