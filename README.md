# CodeIgniter Asset

This package adds a library to your CodeIgniter project in order to easily include assets in your code.

## Installation

### Using composer (recommended)

1. From your CodeIgniter project, type `composer require steevedroz/ci-asset:^1.0`.
1. From the same place, type `php vendor/steevedroz/ci-asset/install.php`. This will copy the required files to your CodeIgniter project (to `application/libraries/Asset.php` and `application/config/asset.php`). **:warning: be sure you don't already have a library named `Asset.php` before running, or your work will be lost!**

### Manually

Merge the `application` folder of this project to your CodeIgniter `application` folder.

## General use

In order to use this library, you first have to load it using

```
$this->load->library('asset');
```

Once it's done, you can use the library in two steps:

1. In a controller, add a given asset to your project.
1. In the `<head>` part of your view, use the corresponding type to display all the lines needed to include your assets.

### CSS

To include a CSS file in a specific document, do the following:

1. Put your file in `/assets/css/`. For example: `/assets/css/example.css`.
1. Use this line **before** you load the view : `$this->asset->css('example');`. (this step can be repeated multiple times)
1. In the `<head>` part of your view, add this line: `<?= $this->asset->css() ?>`

This technique works best if you always have the same HTML header.

### Javascript

To include a Javascript file in a specific document, do the following:

1. Put your file in `/assets/js/`. For example: `/assets/js/example.js`.
1. Use this line **before** you load the view : `$this->asset->js('example');`. (this step can be repeated multiple times)
1. In the `<head>` part of your view, add this line: `<?= $this->asset->js() ?>`

### Example

**Note: `application/config/autoload.php` loads the library `asset`.**

```php
<?php
// application/controllers/Blog.php

class Blog extends CI_Controller
{
    public function index()
    {
        $this->asset->css('blog');
        $this->asset->css('comment');

        $this->asset->js('blog');

        $this->load->view('templates/header');
        $this->load->view('pages/blog/index');
        $this->load->view('templates/footer');
    }
}
```

```php
<!-- application/views/templates/header.php -->

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Blog</title>
        <?= $this->asset->css() ?>
        <?= $this->asset->js() ?>
    </head>
```

Result:

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Blog</title>
        <link rel="stylesheet" href="assets/css/blog.css" type="text/css">
        <link rel="stylesheet" href="assets/css/comment.css" type="text/css">
        <script src="assets/js/blog.js" defer></script>
    </head>
```

## API

Here are all the possible use of the `Asset` library.

### `$asset->css($css = null)`

When `$css` is a string, it adds a CSS to display to the list.

When `$css` isn't specified, it returns all the lines that will include the CSS sheets previously added.

### `$asset->js($js = null, $library = false)`

When `$js` is a string, it adds a Javascript to display to the list.

When `$js` isn't specified, it returns all the lines that will include the Javascripts previously added.

The `$library` parameter specifies if the script is a library (example: jQuery) that must be loaded *before* the DOM is ready. If not specified or set to false, the `defer` keyword will be added to the `<script>` tag.

### `$asset->css_url`

This specifies a custom path for CSS files. The default value is `assets/css/`.

### `$asset->js_url`

This specifies a custom path for Javascript files. The default value is `assets/js/`.

### Default assets

If you have assets that must be loaded by default on all pages, fill `application/config/asset.php` as follow:

The `asset-js` key corresponds to a key-value array containing the name of the script associated with wether it is a library or not.

The `asset-css` key corresponds to an array of stylesheets.

#### Example

```php
<?php

$config['asset-js'] = [
    'script-name' => false,
    'library-name' => true
];

$config['asset-css'] = [
    'style-1',
    'style-2',
    'style-3'
];
```
