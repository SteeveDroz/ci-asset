<?php
defined('BASEPATH') OR exit('No direct script access allowed');

namespace SteeveDroz\CiAsset;

/**
 * This class allows you to easily include assets in your CodeIgniter projet.
 *
 * Please see doc at https://packagist.org/packages/steevedroz/ci-asset for more information.
 *
 * License MIT
 *
 * @author Steeve Droz
 */
class Asset
{
    private $_javascripts;
    private $_stylesheets;

    public $asset_url;
    public $js_url;
    public $css_url;

    public function __construct()
    {
        $CI =& get_instance();

        $CI->load->helper('url');
        $CI->load->config('asset');

        $this->_javascripts = $CI->config->item('asset-js');
        $this->_stylesheets = $CI->config->item('asset-css');

        $this->asset_url = 'assets';
        $this->js_url = 'assets/js';
        $this->css_url = 'assets/css';
    }

    public function js($js = null, $library = false)
    {
        if ($js === null)
        {
            return $this->_echo_js();
        }
        else
        {
            $this->_javascripts[$js] = $library;
        }
    }

    public function css($css = null)
    {
        if ($css === null)
        {
            return $this->_echo_css();
        }
        else
        {
            $this->_stylesheets[] = $css;
        }
    }

    private function _echo_js()
    {
        $that = $this;
        $javascripts = [];
        array_walk($this->_javascripts, function($library, $script) use(&$javascripts, $that)
        {
            $javascripts[] = '<script src="' . base_url($that->js_url) . '/' . $script . '.js"' . ($library ? '' : ' defer') . '></script>';
        });
        return implode(PHP_EOL, $javascripts) . PHP_EOL;
    }

    private function _echo_css()
    {
        $that = $this;
        return implode(PHP_EOL, array_map(function($css) use($that)
        {
            return '<link rel="stylesheet" href="' . base_url($that->css_url) . '/' . $css . '.css" type="text/css">';
        }, $this->_stylesheets)) . PHP_EOL;
    }
}
